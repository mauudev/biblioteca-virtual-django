function validateForm()
{ 
  var form_elements = new Array("nombre_empleado", "paterno_empleado", "materno_empleado", "direccion_empleado");
  var elementAlert = new Array("Te falta el nombre", "Te falta el apellido paterno", "Te falta el apellido materno", "Te falta la dirección");

  for (var i=0;i<form_elements.length;i++)
  {
    var element=document.forms["alta_usuario"][form_elements[i]].value;
    if (element==null || element=="")
    {
      alert(elementAlert[i]);
      return false;
    }
  }
}
