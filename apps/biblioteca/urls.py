from django.conf.urls import patterns, include, url
#from .views import index2,index ELIMINADO PARA LOGIN
from .views import Registrarse

urlpatterns = patterns('',

    # url(r'^$' , index.as_view()),
    # url(r'^index/$' , index2.as_view()),
    #ELIMINADO PARA LOGIN
    #LOGIN
    url(r'^$' , 'django.contrib.auth.views.login',{'template_name':'biblioteca/login.html'},name= 'login'),
    url(r'^cerrar/$' , 'django.contrib.auth.views.logout_then_login', name= 'logout'),
    url(r'^registrarse/$', Registrarse.as_view(),name='registrarse')
)