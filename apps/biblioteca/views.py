from django.views.generic import TemplateView, FormView
from .forms import UserForm
from django.core.urlresolvers import reverse_lazy
from .models import Perfiles

class Registrarse(FormView):
    template_name = 'biblioteca/registrarse.html'
    form_class = UserForm
    success_url = reverse_lazy('registrarse')

    def form_valid(self, form):#si los datos de registro son validos esto se ejecuta
        user = form.save()
        perfil = Perfiles()
        perfil.usuario = user
        perfil.telefono = form.cleaned_data['telefono']
        perfil.save()
        return super(Registrarse, self).form_valid(form)

# from django.shortcuts import render, render_to_response
# from django.views.generic import TemplateView
# from django.template import RequestContext
#
# # def index(request):
# #     return render_to_response('biblioteca/index.html')
# #PARA MOSTRAR CON ESTILO SE ENVIA UNA TEMPLATEVIEW
# class index(TemplateView):
#     template_name = 'biblioteca/index.html'
#
#     # def get(self, request, *args, **kwargs):
#     #     return render_to_response('biblioteca/index.html',
#     #                               context_instance = RequestContext(request))
#     #TODOS ESTO YA TIENE TEMPLATEVIEW
#
# class index2(TemplateView):
#     template_name = 'biblioteca/index2.html'

#ELIMINADO PARA CREAR SISTEMA DE LOGIN

