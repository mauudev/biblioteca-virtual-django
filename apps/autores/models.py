from django.db import models


class Autor(models.Model):
    nombre = models.CharField(max_length=50)
    pais = models.CharField(max_length=50)
    foto = models.ImageField(upload_to='foto_autor')

    def __str__(self):
        return self.nombre