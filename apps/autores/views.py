from django.views.generic import CreateView,TemplateView,ListView
from .models import Autor
from django.core.urlresolvers import reverse_lazy

class RegistrarAutor(CreateView):
    template_name = 'autores/registrarAutor.html'
    model = Autor
    success_url = reverse_lazy('reportar_autor')#redirige a una pagina cuando se haga el envio de datos

class ReportarAutor(ListView):
    template_name = 'autores/reportarAutor.html'
    model = Autor
    #context_object_name = 'autores' -> reemplazar por object_list en la template
#TemplateView -> solo es una vista para redirigir
#ListView Lista de objetos