from django.shortcuts import render
from django.views.generic import TemplateView
from .models import Libro
from .models import Autor
 # template_name = 'libros/buscar.html' #reeplazado por lo de abajo

class BuscarView(TemplateView):

    def post(self, request, *args, **kwargs):
        buscar = request.POST['buscalo']
        libros = Libro.objects.filter(nombre__icontains=buscar)
        if libros:
            datos = []
            for libro in libros:
                autores = libro.autor.all()
                datos.append(dict([(libro,autores)]))
            return render(request,'libros/login.html',{'datos':datos})
        else:
            print("Has preguntado por un autor..")
            autores = Autor.objects.filter(nombre__icontains=buscar)
            return render(request, 'libros/login.html',{'autores':autores, 'autor':True})

























