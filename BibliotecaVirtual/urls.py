from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BibliotecaVirtual.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    #biblioteca
    url(r'^' , include('apps.biblioteca.urls')),
    #autores
    url(r'^autor/' , include('apps.autores.urls')),
    #libros
    url(r'^libros/' , include('apps.libros.urls')),


)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
